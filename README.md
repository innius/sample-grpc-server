# sample-grpc-server 

Sample grpc backend server for testing the [grafana-simple-grpc-datasource](https://github.com/innius/grafana-simple-grpc-datasource).

This release only supports fake data which means it returns a hard coded list of dimension keys, dimension values and metrics. 

