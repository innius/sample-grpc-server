package v2

import (
	"context"
	"time"

	"bitbucket.org/innius/simple-grpc-server/pkg/scenario"
	"bitbucket.org/innius/simple-grpc-server/pkg/scenario/specification"
	"bitbucket.org/innius/simple-grpc-server/pkg/scenario/tsdb"
	v2 "bitbucket.org/innius/simple-grpc-server/proto/v2"
	"github.com/samber/lo"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func NewSampleServerV2(s scenario.IScenario, db tsdb.DB) *SampleServerV2 {
	return &SampleServerV2{
		scenario: s,
		db:       db,
	}

}

type SampleServerV2 struct {
	v2.UnsafeGrafanaQueryAPIServer
	scenario scenario.IScenario
	db       tsdb.DB
}

func (s SampleServerV2) ListDimensionKeys(ctx context.Context, request *v2.ListDimensionKeysRequest) (*v2.ListDimensionKeysResponse, error) {
	keys := lo.Map(s.scenario.ListDimensionKeys(), func(item string, index int) *v2.ListDimensionKeysResponse_Result {
		return &v2.ListDimensionKeysResponse_Result{
			Key:         item,
			Description: item,
		}
	})
	return &v2.ListDimensionKeysResponse{
		Results: keys,
	}, nil
}

func (s SampleServerV2) ListDimensionValues(ctx context.Context, request *v2.ListDimensionValuesRequest) (*v2.ListDimensionValuesResponse, error) {
	keys := lo.Map(s.scenario.ListDimensionValues(request.DimensionKey, request.Filter), func(item string, index int) *v2.ListDimensionValuesResponse_Result {
		return &v2.ListDimensionValuesResponse_Result{
			Value:       item,
			Description: item,
		}
	})
	return &v2.ListDimensionValuesResponse{
		Results: keys,
	}, nil
}

func (s SampleServerV2) ListMetrics(ctx context.Context, request *v2.ListMetricsRequest) (*v2.ListMetricsResponse, error) {
	dims := lo.SliceToMap(request.Dimensions, func(dim *v2.Dimension) (string, string) {
		return dim.Key, dim.Value
	})
	metrics := s.scenario.ListMetrics(dims)
	result := lo.Map(metrics, func(metric scenario.Metric, _ int) *v2.ListMetricsResponse_Metric {
		return &v2.ListMetricsResponse_Metric{
			Name:        metric.Name,
			Description: metric.Description,
		}
	})
	return &v2.ListMetricsResponse{
		Metrics: result,
	}, nil
}

func mapDimensions(dims []*v2.Dimension) map[string]string {
	result := make(map[string]string, len(dims))
	for _, dim := range dims {
		result[dim.Key] = dim.Value
	}
	return result
}

func (s SampleServerV2) GetMetricValue(ctx context.Context, request *v2.GetMetricValueRequest) (*v2.GetMetricValueResponse, error) {
	if len(request.Metrics) == 0 {
		return nil, nil
	}
	var frames []*v2.GetMetricValueResponse_Frame
	for i := range request.Metrics {
		m := request.Metrics[i]
		metric, found := s.scenario.GetMetricByName(mapDimensions(request.Dimensions), m)
		if !found {
			continue
		}
		now := time.Now()
		rows, err := s.db.Select(metric.ID, nil, now.Add(-time.Hour), now.Add(time.Hour), tsdb.Limit(1), tsdb.Descending(), tsdb.Limit(1))
		if err != nil {
			return nil, err
		}
		if len(rows) == 0 {
			continue
		}
		frames = append(frames, &v2.GetMetricValueResponse_Frame{
			Metric:    metric.Name,
			Timestamp: timestamppb.New(rows[0].Timestamp),
			Fields: []*v2.SingleValueField{
				{
					Name: "",
					Labels: lo.MapToSlice(metric.Labels, func(key string, value string) *v2.Label {
						return &v2.Label{Key: key, Value: value}
					}),
					Config: &v2.Config{
						Unit: metric.Unit,
						Mappings: lo.Map(metric.ValueMapping, func(v specification.ValueMapping, _ int) *v2.ValueMapping {
							return &v2.ValueMapping{
								Value: v.Value,
								Text:  v.Text,
							}
						}),
					},
					Value: rows[0].Value,
				},
			},
			Meta: nil,
		})
	}
	return &v2.GetMetricValueResponse{
		Frames: frames,
	}, nil
}

type query interface {
	GetStartDate() *timestamppb.Timestamp
	GetEndDate() *timestamppb.Timestamp
	GetMetrics() []string
	GetDimensions() []*v2.Dimension
}

func (s SampleServerV2) selectData(ctx context.Context, request query, opts ...tsdb.Option) ([]*v2.Frame, error) {
	startDate := request.GetStartDate().AsTime()
	endDate := request.GetEndDate().AsTime()
	metrics := request.GetMetrics()
	if len(metrics) == 0 {
		return nil, nil
	}
	var frames []*v2.Frame
	for i := range metrics {
		m := metrics[i]
		metric, found := s.scenario.GetMetricByName(mapDimensions(request.GetDimensions()), m)
		if !found {
			continue
		}
		rows, err := s.db.Select(metric.ID, nil, startDate, endDate, opts...)
		if err != nil {
			return nil, err
		}
		if len(rows) == 0 {
			continue
		}
		timestamps := lo.Map(rows, func(row tsdb.DataPoint, _ int) *timestamppb.Timestamp {
			return timestamppb.New(row.Timestamp)
		})
		values := lo.Map(rows, func(row tsdb.DataPoint, _ int) float64 {
			return row.Value
		})
		frames = append(frames, &v2.Frame{
			Metric:     metric.Name,
			Timestamps: timestamps,
			Fields: []*v2.Field{
				{
					Name: "",
					Labels: lo.MapToSlice(metric.Labels, func(key string, value string) *v2.Label {
						return &v2.Label{Key: key, Value: value}
					}),
					Config: &v2.Config{
						Unit: metric.Unit,
						Mappings: lo.Map(metric.ValueMapping, func(v specification.ValueMapping, _ int) *v2.ValueMapping {
							return &v2.ValueMapping{
								Value: v.Value,
								Text:  v.Text,
							}
						}),
					},
					Values: values,
				},
			},
			Meta: nil,
		})
	}
	return frames, nil
}

func (s SampleServerV2) GetMetricHistory(ctx context.Context, request *v2.GetMetricHistoryRequest) (*v2.GetMetricHistoryResponse, error) {
	frames, err := s.selectData(ctx, request)
	if err != nil {
		return nil, err
	}
	return &v2.GetMetricHistoryResponse{
		Frames: frames,
	}, nil
}

func (s SampleServerV2) GetMetricAggregate(ctx context.Context, request *v2.GetMetricAggregateRequest) (*v2.GetMetricAggregateResponse, error) {
	interval := time.Duration(request.IntervalMs) * time.Millisecond

	frames, err := s.selectData(ctx, request, tsdb.Aggregate(tsdb.AggregateType(request.AggregateType), interval))
	if err != nil {
		return nil, err
	}
	return &v2.GetMetricAggregateResponse{
		Frames: frames,
	}, nil
}
