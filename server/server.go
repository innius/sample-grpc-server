package server

import (
	pb "bitbucket.org/innius/simple-grpc-server/proto"
	"context"
	"fmt"
	"strings"
	"time"
)

type SampleServer struct {
	pb.UnimplementedGrafanaQueryAPIServer
}

func (s *SampleServer) ListDimensionKeys(ctx context.Context, request *pb.ListDimensionKeysRequest) (*pb.ListDimensionKeysResponse, error) {
	dimensions := &pb.ListDimensionKeysResponse{
		Results: []*pb.ListDimensionKeysResponse_Result{
			{
				Key:         "room",
				Description: "The selected room",
			},
		},
	}
	return dimensions, nil
}

func (s *SampleServer) ListDimensionValues(ctx context.Context, in *pb.ListDimensionValuesRequest) (*pb.ListDimensionValuesResponse, error) {
	var values []*pb.ListDimensionValuesResponse_Result
	for key, value := range map[string]string{
		"living-room": "Living room",
		"bath-room-0": "Bath room ground floor",
		"bath-room-1": "Bath room first floor",
		"kitchen":     "Kitchen",
	} {
		if in.Filter == "" || strings.Contains(key, in.Filter) || strings.Contains(value, in.Filter) {
			values = append(values, &pb.ListDimensionValuesResponse_Result{
				Value:       key,
				Description: value,
			})
		}
	}

	return &pb.ListDimensionValuesResponse{
		Results: values,
	}, nil
}

var sensors = map[string]*Sensor{
	"/living-room/temperature": NewSensorWithRange(21, 22, 60, "℃"),
	"/bath-room-0/temperature": NewSensorWithRange(24.01, 24.9, 60, "℃"),
	"/bath-room-1/temperature": NewSensorWithRange(25.2, 25.7, 60, "℃"),
	"/kitchen/temperature":     NewSensorWithRange(17.5, 18.1, 60, "℃"),

	"/living-room/humidity": NewSensorWithRange(0.6, 0.7, 60, "%"),
	"/bath-room-0/humidity": NewSensorWithRange(0.8, 0.92, 60, "%"),
	"/bath-room-1/humidity": NewSensorWithRange(0.82, 0.89, 60, "%"),
	"/kitchen/humidity":     NewSensorWithRange(0.6, 0.69, 60, "%"),
}

func (s *SampleServer) ListMetrics(ctx context.Context, request *pb.ListMetricsRequest) (*pb.ListMetricsResponse, error) {
	if len(request.Dimensions) < 1 {
		return nil, nil
	}
	room := request.Dimensions[0]
	if room.Value == "" {
		return nil, nil
	}
	metrics := []*pb.ListMetricsResponse_Metric{
		{
			Name:        fmt.Sprintf("/%s/temperature", room.Value),
			Description: "Temperature",
		},
		{
			Name:        fmt.Sprintf("/%s/humidity", room.Value),
			Description: "Humidity",
		},
	}
	return &pb.ListMetricsResponse{
		Metrics: metrics,
	}, nil
}

func (s *SampleServer) GetMetricValue(ctx context.Context, request *pb.GetMetricValueRequest) (*pb.GetMetricValueResponse, error) {
	if request.Metric == "" {
		return nil, nil
	}
	sensor := sensors[request.Metric]
	if sensor == nil {
		return nil, fmt.Errorf("metric %s does not exist; please check your configuration", request.Metric)
	}
	now := time.Now()
	return &pb.GetMetricValueResponse{
		Timestamp: now.Unix(),
		Value: &pb.MetricValue{
			DoubleValue: sensor.GetValueAt(now),
		},
	}, nil

}

func (s *SampleServer) GetMetricHistory(ctx context.Context, request *pb.GetMetricHistoryRequest) (*pb.GetMetricHistoryResponse, error) {
	if request.Metric == "" {
		return nil, nil
	}
	sensor := sensors[request.Metric]
	if sensor == nil {
		return nil, fmt.Errorf("metric %s does not exist; please check your configuration", request.Metric)
	}
	var values []*pb.MetricHistoryValue
	startDate := time.Unix(request.StartDate, 0)
	endDate := time.Unix(request.EndDate, 0)

	for tick := startDate; tick.Before(endDate); tick = tick.Add(time.Second) {
		values = append(values, &pb.MetricHistoryValue{
			Timestamp: tick.Unix(),
			Value: &pb.MetricValue{
				DoubleValue: sensor.GetValueAt(tick),
			},
		})
	}
	return &pb.GetMetricHistoryResponse{
		Values:    values,
		NextToken: "",
	}, nil
}

func (s *SampleServer) GetMetricAggregate(ctx context.Context, request *pb.GetMetricAggregateRequest) (*pb.GetMetricAggregateResponse, error) {
	if request.Metric == "" {
		return nil, nil
	}
	sensor := sensors[request.Metric]
	if sensor == nil {
		return nil, fmt.Errorf("metric %s does not exist; please check your configuration", request.Metric)
	}
	var values []*pb.MetricHistoryValue
	startDate := time.Unix(request.StartDate, 0)
	endDate := time.Unix(request.EndDate, 0)

	for tick := startDate; tick.Before(endDate); tick = tick.Add(10 * time.Second) {
		values = append(values, &pb.MetricHistoryValue{
			Timestamp: tick.Unix(),
			Value: &pb.MetricValue{
				DoubleValue: sensor.GetValueAt(tick), // multiply with an arbitrary factor
			},
		})
	}
	return &pb.GetMetricAggregateResponse{
		Values:    values,
		NextToken: "",
	}, nil
}
