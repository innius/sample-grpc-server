package v3

import (
	"context"
	"errors"
	"time"

	"bitbucket.org/innius/simple-grpc-server/pkg/scenario"
	"bitbucket.org/innius/simple-grpc-server/pkg/scenario/specification"
	"bitbucket.org/innius/simple-grpc-server/pkg/scenario/tsdb"
	v3 "bitbucket.org/innius/simple-grpc-server/proto/v3"
	"github.com/samber/lo"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type SampleServerV3 struct {
	v3.UnsafeGrafanaQueryAPIServer
	scenario scenario.IScenario
	db       tsdb.DB
}

func NewSampleServer(s scenario.IScenario, db tsdb.DB) *SampleServerV3 {
	return &SampleServerV3{
		scenario: s,
		db:       db,
	}
}

const (
	AggregationTypeOption  = "aggregation_type"
	BackendMetadataOption  = "backend_metadata"
	AggregationTypeAverage = "avg"
	AggregationTypeMax     = "max"
	AggregationTypeMin     = "min"
	AggregationTypeCount   = "count"
)

// Gets the options for the specified query type
func (sampleserverv3 *SampleServerV3) GetQueryOptions(ctx context.Context, in *v3.GetOptionsRequest) (*v3.GetOptionsResponse, error) {
	var Options []*v3.Option
	switch in.GetQueryType() {
	case v3.GetOptionsRequest_GetMetricAggregate:
		Options = []*v3.Option{
			{
				Id:          AggregationTypeOption,
				Label:       "Aggregate",
				Description: "Aggregate the query results",
				Type:        v3.Option_Enum,
				EnumValues: []*v3.EnumValue{
					{Label: "Average", Description: "Calculate the average of the values", Id: AggregationTypeAverage},
					{Label: "min", Description: "Calculate the minimum of the values", Id: AggregationTypeMin},
					{Label: "Max", Description: "Calculate the maximum of the values", Id: AggregationTypeMax, Default: true},
					{Label: "Count", Description: "Calculate the sum of the values", Id: AggregationTypeCount},
				},
			},
		}
		// add an additional option only for the Count aggregate type
		if in.GetSelectedOptions()[AggregationTypeOption] == AggregationTypeCount {
			Options = append(Options, &v3.Option{
				Id:          "Limit",
				Label:       "Limit",
				Description: "sample option which demonstrates query options can be made dependent on other options",
				Type:        v3.Option_Enum,
				Required:    true,
				EnumValues: []*v3.EnumValue{
					{Label: "Unlimited", Description: "Unlimited", Id: "unlimited"},
					{Label: "Top10", Description: "Top 10 Only", Id: "top10", Default: true},
				},
			})
		}
	case v3.GetOptionsRequest_GetMetricValue:
	case v3.GetOptionsRequest_GetMetricHistory:
	}
	Options = append(Options, []*v3.Option{
		{Id: BackendMetadataOption, Label: "Use Metric Metadata", Type: v3.Option_Boolean, Description: "Get metrics metadata (e.g. unit of measure) from backend"},
	}...)

	return &v3.GetOptionsResponse{Options: Options}, nil
}

func (s SampleServerV3) ListDimensionKeys(ctx context.Context, request *v3.ListDimensionKeysRequest) (*v3.ListDimensionKeysResponse, error) {
	keys := lo.Map(s.scenario.ListDimensionKeys(), func(item string, index int) *v3.ListDimensionKeysResponse_Result {
		return &v3.ListDimensionKeysResponse_Result{
			Key:         item,
			Description: item,
		}
	})
	return &v3.ListDimensionKeysResponse{
		Results: keys,
	}, nil
}

func (s SampleServerV3) ListDimensionValues(ctx context.Context, request *v3.ListDimensionValuesRequest) (*v3.ListDimensionValuesResponse, error) {
	keys := lo.Map(s.scenario.ListDimensionValues(request.DimensionKey, request.Filter), func(item string, index int) *v3.ListDimensionValuesResponse_Result {
		return &v3.ListDimensionValuesResponse_Result{
			Value:       item,
			Description: item,
		}
	})
	return &v3.ListDimensionValuesResponse{
		Results: keys,
	}, nil
}

func (s SampleServerV3) ListMetrics(ctx context.Context, request *v3.ListMetricsRequest) (*v3.ListMetricsResponse, error) {
	dims := lo.SliceToMap(request.Dimensions, func(dim *v3.Dimension) (string, string) {
		return dim.Key, dim.Value
	})
	metrics := s.scenario.ListMetrics(dims)
	result := lo.Map(metrics, func(metric scenario.Metric, _ int) *v3.ListMetricsResponse_Metric {
		return &v3.ListMetricsResponse_Metric{
			Name:        metric.Name,
			Description: metric.Description,
		}
	})
	return &v3.ListMetricsResponse{
		Metrics: result,
	}, nil
}

func mapDimensions(dims []*v3.Dimension) map[string]string {
	result := make(map[string]string, len(dims))
	for _, dim := range dims {
		result[dim.Key] = dim.Value
	}
	return result
}

func getMetricConfig(metric scenario.Metric) *v3.Config {
	return &v3.Config{
		Unit: metric.Unit,
		Mappings: lo.Map(metric.ValueMapping, func(v specification.ValueMapping, _ int) *v3.ValueMapping {
			return &v3.ValueMapping{
				Value: v.Value,
				Text:  v.Text,
			}
		}),
	}
}

func getConfigFromMetricEnabledOption(options map[string]string) bool {
	s := options[BackendMetadataOption]
	if s == "true" {
		return true
	}
	return false
}

func (s SampleServerV3) GetMetricValue(ctx context.Context, request *v3.GetMetricValueRequest) (*v3.GetMetricValueResponse, error) {
	if len(request.Metrics) == 0 {
		return nil, nil
	}
	var frames []*v3.GetMetricValueResponse_Frame
	configFromMetricEnabled := getConfigFromMetricEnabledOption(request.GetOptions())
	for i := range request.Metrics {
		m := request.Metrics[i]
		metric, found := s.scenario.GetMetricByName(mapDimensions(request.Dimensions), m)
		if !found {
			continue
		}
		rows, err := s.db.Select(metric.ID, nil, request.StartDate.AsTime(), request.EndDate.AsTime(), tsdb.Limit(1), tsdb.Descending(), tsdb.Limit(1))
		if err != nil {
			return nil, err
		}
		if len(rows) == 0 {
			continue
		}
		frames = append(frames, &v3.GetMetricValueResponse_Frame{
			Metric:    metric.Name,
			Timestamp: timestamppb.New(rows[0].Timestamp),
			Fields: []*v3.SingleValueField{
				{
					Name: "",
					Labels: lo.MapToSlice(metric.Labels, func(key string, value string) *v3.Label {
						return &v3.Label{Key: key, Value: value}
					}),
					Config: lo.Ternary(configFromMetricEnabled, getMetricConfig(metric), nil),
					Value:  rows[0].Value,
				},
			},
			Meta: nil,
		})
	}
	return &v3.GetMetricValueResponse{
		Frames: frames,
	}, nil
}

type query interface {
	GetStartDate() *timestamppb.Timestamp
	GetEndDate() *timestamppb.Timestamp
	GetMetrics() []string
	GetDimensions() []*v3.Dimension
	GetOptions() map[string]string
}

func (s SampleServerV3) selectData(ctx context.Context, request query, opts ...tsdb.Option) ([]*v3.Frame, error) {
	configFromMetricEnabled := getConfigFromMetricEnabledOption(request.GetOptions())
	startDate := request.GetStartDate().AsTime()
	endDate := request.GetEndDate().AsTime()
	metrics := request.GetMetrics()
	if len(metrics) == 0 {
		return nil, nil
	}
	var frames []*v3.Frame
	for i := range metrics {
		m := metrics[i]
		metric, found := s.scenario.GetMetricByName(mapDimensions(request.GetDimensions()), m)
		if !found {
			continue
		}
		rows, err := s.db.Select(metric.ID, nil, startDate, endDate, opts...)
		if err != nil {
			return nil, err
		}
		if len(rows) == 0 {
			continue
		}
		timestamps := lo.Map(rows, func(row tsdb.DataPoint, _ int) *timestamppb.Timestamp {
			return timestamppb.New(row.Timestamp)
		})
		values := lo.Map(rows, func(row tsdb.DataPoint, _ int) float64 {
			return row.Value
		})

		frames = append(frames, &v3.Frame{
			Metric:     metric.Name,
			Timestamps: timestamps,
			Fields: []*v3.Field{
				{
					Name: "",
					Labels: lo.MapToSlice(metric.Labels, func(key string, value string) *v3.Label {
						return &v3.Label{Key: key, Value: value}
					}),
					Config: lo.Ternary(configFromMetricEnabled, getMetricConfig(metric), nil),
					Values: values,
				},
			},
			Meta: nil,
		})
	}
	return frames, nil
}

func (s SampleServerV3) GetMetricHistory(ctx context.Context, request *v3.GetMetricHistoryRequest) (*v3.GetMetricHistoryResponse, error) {
	var nextToken string
	if request.StartingToken == "" {
		nextToken = request.GetStartDate().AsTime().Format(time.RFC3339)
	}
	frames, err := s.selectData(ctx, request)
	if err != nil {
		return nil, err
	}
	return &v3.GetMetricHistoryResponse{
		Frames:    frames,
		NextToken: nextToken,
	}, nil
}

func parseAggregateTypeFromRequest(req *v3.GetMetricAggregateRequest) (tsdb.AggregateType, error) {
	s := req.GetOptions()[AggregationTypeOption]
	if s == "" {
		return tsdb.AggregateType_AVERAGE, nil
	}
	switch s {
	case AggregationTypeAverage:
		return tsdb.AggregateType_AVERAGE, nil
	case AggregationTypeMax:
		return tsdb.AggregateType_MAX, nil
	case AggregationTypeMin:
		return tsdb.AggregateType_MIN, nil
	case AggregationTypeCount:
		return tsdb.AggregateType_COUNT, nil
	}
	return tsdb.AggregateType_AVERAGE, errors.New("invalid aggregation type")
}

func (s SampleServerV3) GetMetricAggregate(ctx context.Context, request *v3.GetMetricAggregateRequest) (*v3.GetMetricAggregateResponse, error) {
	interval := lo.Clamp(time.Minute, time.Duration(request.IntervalMs)*time.Millisecond, time.Hour)
	at, err := parseAggregateTypeFromRequest(request)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, "invalid aggregation type")
	}
	// get the aggregateType from the request
	frames, err := s.selectData(ctx, request, tsdb.Aggregate(at, interval))
	if err != nil {
		return nil, err
	}
	return &v3.GetMetricAggregateResponse{
		Frames: frames,
	}, nil
}
