package server

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestSensorValues(t *testing.T) {
	s := &Sensor{
		Values: []float64{
			1, 2, 3,
		},
	}

	tests := []struct {
		sec int
		exp float64
	}{
		{0, 1},
		{1, 2},
		{2, 3},
		{3, 1},
		{4, 2},
	}

	for _, tst := range tests {
		ts := time.Date(2021, 07, 12, 9, 30, tst.sec, 0, time.Local)
		assert.Equal(t, tst.exp, s.GetValueAt(ts))

	}

}
