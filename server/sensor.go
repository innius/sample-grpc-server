package server

import (
	"math/rand"
	"time"
)

type Sensor struct {
	Unit   string
	Values []float64
}

var random = rand.New(rand.NewSource(time.Now().UnixNano()))

func NewSensorWithRange(min, max float64, count int, unit string) *Sensor {
	values := make([]float64, count)

	for i := 0; i < count; i++ {
		values[i] = Float64InRange(min, max)
	}
	return &Sensor{
		Values: values,
		Unit:   unit,
	}
}

func (s *Sensor) GetValueAt(t time.Time) float64 {
	values := s.Values
	l := len(values)
	idx := t.Second() % l

	return values[idx]
}

// Float64InRange will build a random float64 between min and max included.
func Float64InRange(min, max float64) float64 {
	if min >= max {
		return min
	}
	return random.Float64()*(max-min) + min
}
