package main

import (
	"context"
	"flag"
	"io"
	"log"
	"net"
	"os"

	"bitbucket.org/innius/simple-grpc-server/pkg/scenario"
	"bitbucket.org/innius/simple-grpc-server/pkg/scenario/specification"
	"bitbucket.org/innius/simple-grpc-server/pkg/scenario/tsdb"

	pb "bitbucket.org/innius/simple-grpc-server/proto"
	v2 "bitbucket.org/innius/simple-grpc-server/proto/v2"
	v3 "bitbucket.org/innius/simple-grpc-server/proto/v3"
	"bitbucket.org/innius/simple-grpc-server/server"
	serverv2 "bitbucket.org/innius/simple-grpc-server/server/v2"
	serverv3 "bitbucket.org/innius/simple-grpc-server/server/v3"
	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_logrus "github.com/grpc-ecosystem/go-grpc-middleware/logging/logrus"
	"github.com/sirupsen/logrus"
	"golang.org/x/time/rate"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

const port = ":50051"

type MyLimiter struct {
	*rate.Limiter
}

func (m *MyLimiter) Limit() bool {
	return !m.Allow()
}

func ServerInitialization(sc scenario.IScenario, db tsdb.DB, version string) {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	logger := logrus.New()
	s := grpc.NewServer(grpc_middleware.WithUnaryServerChain(grpc_logrus.UnaryServerInterceptor(logrus.NewEntry(logger))))
	reflection.Register(s)
	switch version {
	case "v1":
		log.Println("running v1 server")
		pb.RegisterGrafanaQueryAPIServer(s, &server.SampleServer{})
	case "v2":
		log.Println("running v2 server")
		v2.RegisterGrafanaQueryAPIServer(s, serverv2.NewSampleServerV2(sc, db))
	default:
		log.Println("running v3 server")
		v3.RegisterGrafanaQueryAPIServer(s, serverv3.NewSampleServer(sc, db))
	}

	log.Printf("start grpc server at %s\n", port)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}

func main() {
	var spec string
	version := "v3"
	flag.StringVar(&spec, "spec", "", "a custom yml specification")
	flag.StringVar(&version, "version", version, "Version of the server")
	flag.Parse()
	var s *specification.Scenario
	var err error
	if spec != "" {
		f, err := os.Open(spec)
		if err != nil {
			log.Fatal(err)
		}
		defer f.Close()
		b, err := io.ReadAll(f)
		if err != nil {
			log.Fatal(err)
		}
		s, err = specification.NewFromConfig(b)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		s, err = specification.New()
	}
	if err != nil {
		log.Fatalf("failed to initialize specification: %v", err)
	}

	db, err := tsdb.New()
	if err != nil {
		log.Fatalf("failed to open tsdb: %v", err)
	}
	sc := scenario.New(*s, db)
	go func() {
		ctx := context.Background()
		err := sc.Run(ctx)
		if err != nil {
			log.Fatalf("failed to run scenario: %v", err)
		}
	}()
	ServerInitialization(sc, db, version)
}
