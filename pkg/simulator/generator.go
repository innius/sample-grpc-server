package simulator

import (
	"time"

	"gonum.org/v1/gonum/stat/distuv"
)

type Sampler interface {
	GetInterval() time.Duration
	Random() float64
}

type metricSampler struct {
	m          Metric
	freq       distuv.Normal
	dist       distuv.Normal
	randomFunc func() float64
}

func (s metricSampler) GetInterval() time.Duration {
	return time.Duration(s.freq.Rand())
}

func (s metricSampler) Random() float64 {
	return s.randomFunc()
}

func getRandomFuncForMetricValue(v Value) func() float64 {
	if len(v.Sequence) > 0 {
		return (sequence(v.Sequence...))
	}
	dist := distuv.Normal{
		Mu:    v.Mean,
		Sigma: v.StdDev,
	}
	return dist.Rand
}

func sequence(values ...float64) func() float64 {
	idx := 0
	return func() float64 {
		if idx >= len(values) {
			idx = 0
		}
		idx++
		return values[idx-1]
	}
}

func newSampler(m Metric) Sampler {
	return metricSampler{
		m: m,
		freq: distuv.Normal{
			Mu:    float64(m.Frequency.Mean),
			Sigma: float64(m.Frequency.StdDev),
		},
		randomFunc: getRandomFuncForMetricValue(m.Value),
	}
}

// func NewGenerator(ctx context.Context, s Sampler) func(yield func(Sample)) {
// 	return func(yield func(Sample)) {
// 		for {
// 			select {
// 			case <-ctx.Done():
// 				return
// 			default:
// 				yield(Sample{
// 					Value:     s.Random(),
// 					Timestamp: time.Now(),
// 				})
// 				time.Sleep(s.GetInterval())
// 			}
// 		}
// 	}
// }
