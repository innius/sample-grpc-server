package simulator

import "time"

type Frequency struct {
	Mean   time.Duration
	StdDev time.Duration
}

type Value struct {
	Sequence []float64 `yaml:"sequence"`
	Mean     float64
	StdDev   float64
}

type Metric struct {
	ID        int    `yaml:"_"`
	Name      string `yaml:"name"`
	Frequency Frequency
	Value     Value
}

func (m Metric) getFrequency() time.Duration {

}

type Sample struct {
	ID        int
	Timestamp time.Time
	Value     float64
}
