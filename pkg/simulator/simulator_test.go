package foo

import (
	"context"
	"log"
	"testing"
	"time"

	"github.com/samber/lo"
	"github.com/stretchr/testify/assert"
)

func TestFoo(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	gen := NewFoo(ctx, newSampler(Metric{
		ID:   0,
		Name: "",
		Frequency: Frequency{
			Mean:   20 * time.Millisecond,
			StdDev: 10 * time.Millisecond,
		},
		Value: Value{
			Sequence: []float64{},
			Mean:     20,
			StdDev:   0.3,
		},
	}))

	res := lo.Async(func() []Sample {
		return lo.ChannelToSlice(lo.Generator(10, gen))
	})
	time.AfterFunc(1*time.Second, cancel)
	values := <-res

	log.Println(values)
	assert.Equal(t, 101, len(values))
}
