package simulator

import (
	"context"

	"github.com/samber/lo"
)

type generator func(yield func(Sample))

func NewBar(ctx context.Context, metrics []types.Metric) <-chan types.Sample {
	generators := lo.Map(metrics, func(m types.Metric, _ int) generator {
		return generator.NewGenerator(ctx, newSampler(m))
	})

	upstreams := lo.Map(generators, func(g generator, _ int) <-chan Sample {
		return lo.Generator(100, g)
	})

	return lo.ChannelMerge(100*len(metrics), upstreams...)
}
