package scenario

import (
	"context"

	"bitbucket.org/innius/simple-grpc-server/pkg/scenario/runner"
	"bitbucket.org/innius/simple-grpc-server/pkg/scenario/simulator"
	"bitbucket.org/innius/simple-grpc-server/pkg/scenario/specification"
	"bitbucket.org/innius/simple-grpc-server/pkg/scenario/tsdb"
	"github.com/samber/lo"
)

func New(spec specification.Scenario, db tsdb.DB) IScenario {
	return &Scenario{
		spec: spec,
		db:   db,
	}

}

type Scenario struct {
	spec specification.Scenario
	db   tsdb.DB
}

func (s *Scenario) Run(ctx context.Context) error {
	sim := simulator.New(s.spec)
	r := runner.NewScenarioRunner(sim, s.db)
	return r.Run(ctx)
}

func (s *Scenario) ListDimensionKeys() []string {
	dimensions := lo.FlatMap(s.spec.Metrics, func(item specification.Metric, index int) []specification.Dimension {
		return item.Dimensions
	})

	return lo.Uniq(lo.Map(dimensions, func(item specification.Dimension, index int) string {
		return item.Key
	}))
}

func (s *Scenario) ListDimensionValues(key string, filter string) []string {
	dimensions := lo.FlatMap(s.spec.Metrics, func(item specification.Metric, index int) []specification.Dimension {
		return item.Dimensions
	})

	dimValues := lo.Filter(dimensions, func(item specification.Dimension, index int) bool {
		return item.Key == key
	})
	result := lo.Uniq(lo.Map(dimValues, func(item specification.Dimension, index int) string {
		return item.Value
	}))
	return result
}

type Metric specification.Metric

func (s *Scenario) GetMetricByName(dimensions map[string]string, metric string) (Metric, bool) {
	metrics := s.ListMetrics(dimensions)
	return lo.Find(metrics, func(m Metric) bool {
		return m.Name == metric
	})
}

func (s *Scenario) ListMetrics(dimensions map[string]string) []Metric {
	metrics := lo.Filter(s.spec.Metrics, func(item specification.Metric, index int) bool {
		return lo.ContainsBy(item.Dimensions, func(dim specification.Dimension) bool {
			if v, ok := dimensions[dim.Key]; ok {
				return v == dim.Value
			}
			return false
		})
	})
	return lo.Map(metrics, func(item specification.Metric, _ int) Metric {
		return Metric(item)
	})
}
