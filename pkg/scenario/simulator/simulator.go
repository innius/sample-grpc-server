package simulator

import (
	"context"
	"time"

	"bitbucket.org/innius/simple-grpc-server/pkg/scenario/simulator/generator"
	scenario2 "bitbucket.org/innius/simple-grpc-server/pkg/scenario/specification"
	"github.com/samber/lo"
)

type Simulator struct {
	scenario scenario2.Scenario
}

func New(scenario scenario2.Scenario) ISimulator {
	return &Simulator{scenario: scenario}
}

type TimestamptedValue struct {
	Timestamp time.Time
	Value     float64
	Metric    scenario2.Metric
}

type ISimulator interface {
	Run(ctx context.Context) (<-chan TimestamptedValue, error)
}

type metricWithGenerator struct {
	scenario2.Metric
	*generator.Generator[float64]
}

func newMetricWithGenerator(m scenario2.Metric) metricWithGenerator {
	s := generator.FixedValue(-1)
	if m.Range != nil {
		s = generator.Float64Range(m.Range.Min, m.Range.Max)
	}
	if len(m.Sequence) != 0 {
		s = generator.Sequence(m.Sequence...)
	}
	return metricWithGenerator{Metric: m, Generator: generator.NewGenerator(s)}
}

func getFrequency(m metricWithGenerator) time.Duration {
	if m.Frequency != nil {
		return *m.Frequency
	}
	if m.FrequencyRange != nil {
		gen := generator.NewGenerator(generator.DurationRange(m.FrequencyRange.Min, m.FrequencyRange.Max))
		return gen.Yield()
	}
	return time.Second
}

func (s *Simulator) Run(ctx context.Context) (<-chan TimestamptedValue, error) {
	metrics := lo.Map(s.scenario.Metrics, func(item scenario2.Metric, index int) metricWithGenerator {
		return newMetricWithGenerator(item)
	})
	rc := make(chan TimestamptedValue, len(metrics))
	go func() {
		<-ctx.Done()
		close(rc)
	}()
	for _, metric := range metrics {
		go func(metric metricWithGenerator) {
			now := time.Now()
			start := now.Add(-metric.History)
			freq := getFrequency(metric)
			for tick := start; tick.Before(now); tick = tick.Add(freq) {
				rc <- TimestamptedValue{Timestamp: tick, Value: metric.Yield(), Metric: metric.Metric}
			}
			tc := time.NewTicker(freq)
			for {
				select {
				case tick := <-tc.C:
					rc <- TimestamptedValue{Timestamp: tick, Value: metric.Yield(), Metric: metric.Metric}
					if newFreq := getFrequency(metric); newFreq != freq {
						freq = newFreq
						if tc != nil {
							tc.Reset(freq)
						}
					}
				case <-ctx.Done():
					tc.Stop()
					return
				}
			}
		}(metric)
	}
	return rc, nil
}
