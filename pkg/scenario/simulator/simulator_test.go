package simulator

import (
	"context"
	"testing"
	"time"

	"bitbucket.org/innius/simple-grpc-server/pkg/scenario/specification"
	"github.com/samber/lo"
	"github.com/stretchr/testify/assert"
)

func TestSimulator_Run(t *testing.T) {
	sim := &Simulator{
		scenario: specification.Scenario{
			Metrics: []specification.Metric{
				{
					Name:      "test",
					Frequency: lo.ToPtr(time.Millisecond),
					Range: &specification.Range{
						Min: 10,
						Max: 11,
					},
				},
			},
		},
	}
	ctx, cancel := context.WithTimeout(context.Background(), 100*time.Millisecond)
	stream, err := sim.Run(ctx)
	assert.NoError(t, err)
	assert.NotNil(t, stream)
	counter := 0

	res := lo.Async(func() []TimestamptedValue {
		return lo.ChannelToSlice(stream)
	})
	cancel()
	values := <-res
	lo.ForEach(values, func(v TimestamptedValue, _ int) {
		assert.NotEqual(t, time.Time{}, v.Timestamp)
		assert.NotZero(t, v.Value)
		assert.NotEmpty(t, v.Metric)
		counter++

	})
	assert.NotZero(t, counter)
	t.Run("with historical data", func(t *testing.T) {
		sim := &Simulator{
			scenario: specification.Scenario{
				Metrics: []specification.Metric{
					{
						Name:      "test",
						Frequency: lo.ToPtr(time.Millisecond),
						History:   time.Second,
						Range: &specification.Range{
							Min: 10,
							Max: 11,
						},
					},
				},
			},
		}
		ctx, cancel := context.WithTimeout(context.Background(), 100*time.Millisecond)
		stream, err := sim.Run(ctx)
		assert.NoError(t, err)
		assert.NotNil(t, stream)
		var counter int64 = 0

		res := lo.Async(func() []TimestamptedValue {
			return lo.ChannelToSlice(stream)
		})
		cancel()
		values := <-res
		lo.ForEach(values, func(v TimestamptedValue, _ int) {
			assert.NotEqual(t, time.Time{}, v.Timestamp)
			assert.NotZero(t, v.Value)
			assert.NotEmpty(t, v.Metric)
			counter++
		})
		assert.Greater(t, counter, sim.scenario.Metrics[0].History.Milliseconds())

	})
}
