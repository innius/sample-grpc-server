package generator

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestDurationRange(t *testing.T) {
	res := durationInRange(1*time.Second, 10*time.Second)
	assert.Greater(t, res, 1*time.Second)
	assert.Less(t, res, 10*time.Second)
}
