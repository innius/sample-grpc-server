package generator

func Sequence(values ...float64) Strategy[float64] {
	idx := 0
	return func() float64 {
		if idx >= len(values) {
			idx = 0
		}
		idx++
		return values[idx-1]
	}
}
