package generator

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFloat64Strategy(t *testing.T) {
	res := float64InRange(0.7, 0.72)
	assert.Greater(t, res, 0.7)
}
