package generator

import (
	"math/rand"
	"time"
)

var random = rand.New(rand.NewSource(time.Now().UnixNano()))

func durationInRange(min, max time.Duration) time.Duration {
	if min >= max {
		return min
	}
	delta := max - min
	return time.Duration(random.Intn(int(delta.Seconds())))*time.Second + min
}

func DurationRange(min, max time.Duration) Strategy[time.Duration] {
	return func() time.Duration {
		return durationInRange(min, max)
	}
}

// Float64InRange will build a random float64 between min and max included.
func float64InRange(min, max float64) float64 {
	if min >= max {
		return min
	}
	return random.Float64()*(max-min) + min
}

type Strategy[T float64 | time.Duration] func() T

func Float64Range(min, max float64) Strategy[float64] {
	return func() float64 {
		return float64InRange(min, max)
	}
}

type Generator[T float64 | time.Duration] struct {
	yield Strategy[T]
}

func NewGenerator[T float64 | time.Duration](s Strategy[T]) *Generator[T] {
	return &Generator[T]{yield: s}
}

func (g *Generator[T]) Yield() T {
	return g.yield()
}
