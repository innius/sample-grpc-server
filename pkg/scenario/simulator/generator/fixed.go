package generator

func FixedValue(v float64) Strategy[float64] {
	return func() float64 {
		return v
	}
}
