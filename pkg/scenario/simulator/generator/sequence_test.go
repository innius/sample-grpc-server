package generator

import (
	"testing"

	"github.com/samber/lo"
	"github.com/stretchr/testify/assert"
)

func TestSequence(t *testing.T) {
	values := []float64{1, 2, 2, 3}
	r := Sequence(values...)
	lo.ForEach(values, func(v float64, _ int) {
		assert.Equal(t, v, r())
	})
	assert.Equal(t, 1.0, r())
}
