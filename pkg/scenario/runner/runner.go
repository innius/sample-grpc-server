package runner

import (
	"context"
	"time"

	"bitbucket.org/innius/simple-grpc-server/pkg/scenario/simulator"
	"bitbucket.org/innius/simple-grpc-server/pkg/scenario/tsdb"
	"github.com/jonboulle/clockwork"
	"github.com/samber/lo"
)

const defaultBatchSize = 1000

type ScenarioRunner struct {
	sim          simulator.ISimulator
	db           tsdb.DB
	batchSize    int
	clock        clockwork.Clock
	minFrequency time.Duration
}

func NewScenarioRunner(sim simulator.ISimulator, db tsdb.DB) *ScenarioRunner {
	return &ScenarioRunner{sim: sim, db: db, batchSize: 1, clock: clockwork.NewRealClock(), minFrequency: 500 * time.Millisecond}
}

func (s *ScenarioRunner) Run(ctx context.Context) error {
	stream, err := s.sim.Run(ctx)
	if err != nil {
		return err
	}
	batchSize := s.batchSize
	if batchSize == 0 {
		batchSize = defaultBatchSize
	}
	batch := make([]tsdb.Row, batchSize)
	idx := 0
	t := s.clock.Now()
	for v := range stream {
		err := s.db.InsertRows(lo.Slice(batch, 0, idx))
		if err != nil {
			return err
		}
		batch[idx] = tsdb.Row{
			MetricID: v.Metric.ID,
			Labels:   nil,
			DataPoint: tsdb.DataPoint{
				Value:     v.Value,
				Timestamp: v.Timestamp,
			},
		}
		idx++
		if idx == batchSize || s.clock.Now().Sub(t) > s.minFrequency {
			err := s.db.InsertRows(lo.Slice(batch, 0, idx))
			if err != nil {
				return err
			}
			idx = 0
			batch = make([]tsdb.Row, s.batchSize)
			t = s.clock.Now()
		}
	}
	return nil
}
