package runner

import (
	"context"
	"io"
	"testing"
	"time"

	"bitbucket.org/innius/simple-grpc-server/pkg/scenario/simulator"
	"bitbucket.org/innius/simple-grpc-server/pkg/scenario/specification"
	"bitbucket.org/innius/simple-grpc-server/pkg/scenario/tsdb"
	"github.com/jonboulle/clockwork"
	"github.com/samber/lo"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type TestSimulator struct {
	*TestStream
}

func (t *TestSimulator) Run(ctx context.Context) (simulator.Stream, error) {
	return t.TestStream, nil
}

type TestStream struct {
	values []simulator.TimestamptedValue
	idx    int
}

func (t *TestStream) Recv() (simulator.TimestamptedValue, error) {
	if t.idx >= len(t.values) {
		return simulator.TimestamptedValue{}, io.EOF
	}
	v := t.values[t.idx]
	t.idx++
	return v, nil
}

func (t *TestStream) Close() error {
	return nil
}

func newTestSimulator(values []simulator.TimestamptedValue) *TestSimulator {
	return &TestSimulator{
		TestStream: &TestStream{
			values: values,
		},
	}
}

type mockDB struct {
	mock.Mock
}

func (m *mockDB) Select(id int, labels []tsdb.Label, start, end time.Time, options ...tsdb.Option) ([]tsdb.DataPoint, error) {
	//TODO implement me
	panic("implement me")
}

func (m *mockDB) Close() error {
	//TODO implement me
	panic("implement me")
}

func (m *mockDB) InsertRows(rows []tsdb.Row) error {
	args := m.Called(rows)
	return args.Error(0)
}

func TestScenarioRunner_Run(t *testing.T) {
	m := &mockDB{}
	expected := []tsdb.Row{
		{MetricID: 2, DataPoint: tsdb.DataPoint{Value: 1.0, Timestamp: mustParse("2022-10-27T12:04:05Z")}},
		{MetricID: 2, DataPoint: tsdb.DataPoint{Value: 2.0, Timestamp: mustParse("2022-10-27T12:04:05Z")}},
		{MetricID: 2, DataPoint: tsdb.DataPoint{Value: 3.0, Timestamp: mustParse("2022-10-27T12:04:06Z")}},
		{MetricID: 2, DataPoint: tsdb.DataPoint{Value: 4.0, Timestamp: mustParse("2022-10-27T12:04:08Z")}},
	}
	sim := newTestSimulator(lo.Map(expected, func(row tsdb.Row, index int) simulator.TimestamptedValue {
		return simulator.TimestamptedValue{
			Timestamp: row.Timestamp,
			Value:     row.Value,
			Metric:    specification.Metric{ID: 2},
		}
	}))
	m.On("InsertRows", expected).Return(nil).Once()
	sut := ScenarioRunner{
		db:           m,
		sim:          sim,
		clock:        clockwork.NewRealClock(),
		minFrequency: time.Second,
	}
	ctx := context.Background()
	assert.NoError(t, sut.Run(ctx))
	m.AssertExpectations(t)
	t.Run("process rows in batches", func(t *testing.T) {
		m := &mockDB{}
		m.On("InsertRows", mock.Anything).Return(nil).Times(len(expected) + 1)
		sim := newTestSimulator(lo.Map(expected, func(row tsdb.Row, index int) simulator.TimestamptedValue {
			return simulator.TimestamptedValue{
				Timestamp: row.Timestamp,
				Value:     row.Value,
				Metric:    specification.Metric{ID: row.MetricID},
			}
		}))
		sut = ScenarioRunner{
			db:        m,
			sim:       sim,
			batchSize: 1,
			clock:     clockwork.NewRealClock(),
		}
		assert.NoError(t, sut.Run(ctx))
		m.AssertExpectations(t)
	})
}

func mustParse(s string) time.Time {
	ts, err := time.Parse(time.RFC3339, s)
	if err != nil {
		panic(err)
	}
	return ts
}
