package specification

import (
	_ "embed"
	"time"

	"github.com/samber/lo"
	"gopkg.in/yaml.v3"
)

//go:embed config.yaml
var config []byte

func NewFromConfig(b []byte) (*Scenario, error) {
	var scenario Scenario
	if err := yaml.Unmarshal(b, &scenario); err != nil {
		return nil, err
	}
	scenario.Metrics = lo.Map(scenario.Metrics, func(m Metric, i int) Metric {
		m.ID = i
		return m
	})
	for _, m := range scenario.Metrics {
		if err := lo.Validate(m.Frequency == nil || (m.Frequency != nil && *m.Frequency > time.Millisecond), "frequency must be greater than 1msec"); err != nil {
			return nil, err
		}
		if err := lo.Validate(m.Range == nil || (m.Range != nil && m.Range.Min > 0 || m.Range.Max > 0), "thresholds must be greater than 0"); err != nil {
			return nil, err
		}
	}
	return &scenario, nil

}
func New() (*Scenario, error) {
	return NewFromConfig(config)
}

type Scenario struct {
	Metrics []Metric `yaml:"metrics"`
}

type Range struct {
	Min float64 `yaml:"min"`
	Max float64 `yaml:"max"`
}

type Dimension struct {
	Key   string `yaml:"key"`
	Value string `yaml:"value"`
}

type FrequencyRange struct {
	Min time.Duration `yaml:"min"`
	Max time.Duration `yaml:"max"`
}
type ValueMapping struct {
	Value string `yaml:"value"`
	Text  string `yaml:"text"`
}

// Metric struct  
type Metric struct {
	ID             int               `yaml:"_"`
	Name           string            `yaml:"name"`
	Description    string            `yaml:"description"`
	Unit           string            `yaml:"unit"`
	Frequency      *time.Duration    `yaml:"frequency"`
	FrequencyRange *FrequencyRange   `yaml:"frequency_range"`
	Range          *Range            `yaml:"range"`
	Sequence       []float64         `yaml:"sequence"`
	Labels         map[string]string `yaml:"labels"`
	Dimensions     []Dimension       `yaml:"dimensions"`
	Notes          map[string]string `yaml:"notes"`
	History        time.Duration     `yaml:"history"`
	ValueMapping   []ValueMapping    `yaml:"value_mapping"`
}
