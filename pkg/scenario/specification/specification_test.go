package specification

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestNew(t *testing.T) {
	sut, err := New()
	assert.NoError(t, err)
	assert.NotNil(t, sut)

	assert.Len(t, sut.Metrics, 4)
	assert.Equal(t, 1, sut.Metrics[1].ID)
	assert.Equal(t, "temperature", sut.Metrics[0].Name)
	assert.Equal(t, "℃", sut.Metrics[0].Unit)
	assert.Equal(t, time.Second, *sut.Metrics[0].Frequency)
	assert.Equal(t, 4*time.Hour, sut.Metrics[0].History)
	assert.Equal(t, "temperature sensor", sut.Metrics[0].Description)
	assert.Equal(t, &Range{Min: 18, Max: 19}, sut.Metrics[0].Range)
	assert.NotEmpty(t, sut.Metrics[2].Sequence)
	assert.NotNil(t, sut.Metrics[2].FrequencyRange)
	assert.Equal(t, 30*time.Second, sut.Metrics[2].FrequencyRange.Min)
	assert.Equal(t, time.Minute, sut.Metrics[2].FrequencyRange.Max)
	assert.Equal(t, map[string]string{"zone": "ceiling"}, sut.Metrics[0].Labels)
	assert.Len(t, sut.Metrics[2].ValueMapping, 4)
	assert.Len(t, sut.Metrics[0].Dimensions, 1)
	assert.Equal(t, sut.Metrics[0].Dimensions[0].Key, "room")
	assert.Equal(t, sut.Metrics[0].Dimensions[0].Value, "bathroom")

}
