package tsdb

import (
	"log"
	"strconv"
	"time"

	"github.com/nakabonne/tstorage"
	"github.com/samber/lo"
)

type Label struct{}

// Row includes a data point along with properties to identify a kind of metrics.
type Row struct {
	// The unique name of metric.
	// This field must be set.
	MetricID int
	// An optional key-value properties to further detailed identification.
	Labels []Label
	// This field must be set.
	DataPoint
}

// DataPoint represents a data point, the smallest unit of time series data.
type DataPoint struct {
	Value     float64
	Timestamp time.Time
}

type AggregateType int32

const (
	AggregateType_AVERAGE AggregateType = 0
	AggregateType_MAX     AggregateType = 1
	AggregateType_MIN     AggregateType = 2
	AggregateType_COUNT   AggregateType = 3
)

type config struct {
	Limit         int
	Descending    bool
	Interval      time.Duration
	AggregateType *AggregateType
}

type Option func(*config)

func Limit(n int) Option {
	return func(o *config) {
		o.Limit = n
	}
}
func Descending() Option {
	return func(o *config) {
		o.Descending = true
	}
}

func Aggregate(aggregateType AggregateType, interval time.Duration) Option {
	return func(o *config) {
		o.AggregateType = &aggregateType
		o.Interval = interval
	}
}

type DB interface {
	InsertRows(rows []Row) error
	Select(id int, labels []Label, start, end time.Time, options ...Option) ([]DataPoint, error)
	Close() error
}

type dbImpl struct {
	tstorage.Storage
}

func (d *dbImpl) InsertRows(rows []Row) error {
	return d.Storage.InsertRows(lo.Map(rows, func(row Row, _ int) tstorage.Row {
		return tstorage.Row{
			Metric: strconv.Itoa(row.MetricID),
			Labels: lo.Map(row.Labels, func(item Label, index int) tstorage.Label {
				return tstorage.Label{}
			}),
			DataPoint: tstorage.DataPoint{
				Value:     row.DataPoint.Value,
				Timestamp: row.DataPoint.Timestamp.Unix(),
			},
		}
	}))
}

func (d *dbImpl) Select(id int, labels []Label, start, end time.Time, opts ...Option) ([]DataPoint, error) {
	res, err := d.dbSelect(id, labels, start, end)
	if err != nil {
		return nil, err
	}
	c := config{}
	for _, opt := range opts {
		opt(&c)
	}
	if c.Descending {
		res = lo.Reverse(res)
	}
	if c.Limit > 0 && len(res) > c.Limit {
		return lo.Slice(res, 0, c.Limit), nil
	}

	if c.AggregateType != nil {
		return aggregate(res, start, end, c.Interval, *c.AggregateType)
	}
	return res, nil
}

func aggregate(points []DataPoint, start, end time.Time, interval time.Duration, at AggregateType) ([]DataPoint, error) {
	var res []DataPoint
	for ts := start; ts.Before(end); ts = ts.Add(interval) {
		inRange := lo.Filter(points, func(p DataPoint, _ int) bool {
			return p.Timestamp.Unix() >= ts.Unix() && p.Timestamp.Unix() < ts.Add(interval).Unix()
		})
		if len(inRange) == 0 {
			continue
		}
		vs := lo.Map(inRange, func(item DataPoint, index int) float64 {
			return item.Value
		})

		switch at {
		case AggregateType_AVERAGE:
			avg := lo.Sum(vs) / float64(len(vs))
			res = append(res, DataPoint{
				Value:     avg,
				Timestamp: ts,
			})
		case AggregateType_MAX:
			max := lo.Max(vs)
			res = append(res, DataPoint{
				Value:     max,
				Timestamp: ts,
			})
		case AggregateType_MIN:
			min := lo.Min(vs)
			res = append(res, DataPoint{
				Value:     min,
				Timestamp: ts,
			})
		case AggregateType_COUNT:
			log.Printf("count %d", len(vs))
			res = append(res, DataPoint{
				Value:     float64(len(vs)),
				Timestamp: ts,
			})
		}
	}
	return res, nil
}

func (d *dbImpl) dbSelect(id int, labels []Label, start, end time.Time) ([]DataPoint, error) {
	res, err := d.Storage.Select(strconv.Itoa(id), lo.Map(labels, func(l Label, _ int) tstorage.Label { return tstorage.Label{} }), start.Unix(), end.Unix())
	if err != nil {
		return nil, err
	}
	return lo.Map(res, func(dp *tstorage.DataPoint, _ int) DataPoint {
		return DataPoint{
			Value:     dp.Value,
			Timestamp: time.Unix(dp.Timestamp, 0),
		}
	}), nil
}

func New() (DB, error) {
	storage, err := tstorage.NewStorage(
		tstorage.WithTimestampPrecision(tstorage.Seconds),
		tstorage.WithPartitionDuration(8*time.Hour),
	)
	if err != nil {
		return nil, err
	}
	return &dbImpl{Storage: storage}, nil
}
