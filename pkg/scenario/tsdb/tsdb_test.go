package tsdb

import (
	"testing"
	"time"

	"github.com/samber/lo"
	"github.com/stretchr/testify/assert"
)

func TestSelectAverage(t *testing.T) {
	db, err := New()
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()
	start := time.Now()
	rows := lo.Times(100, func(i int) Row {
		return Row{
			MetricID: 2,
			Labels:   nil,
			DataPoint: DataPoint{
				Value:     float64(i),
				Timestamp: start.Add(time.Duration(i) * time.Second),
			},
		}
	})
	err = db.InsertRows(rows)
	if err != nil {
		t.Fatal(err)
	}
	res, err := db.Select(2, nil, start, start.Add(time.Minute), Aggregate(AggregateType_AVERAGE, 10*time.Second))
	assert.NoError(t, err)
	assert.Len(t, res, 6)
	assert.Equal(t, DataPoint{Value: 4.5, Timestamp: start}, res[0])
}

func TestInsertMultipleRows(t *testing.T) {
	db, err := New()
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()
	now := time.Now()
	start := now.Add(-24 * time.Hour)
	counter := 0
	for tick := start; tick.Before(now); tick = tick.Add(time.Second) {
		err = db.InsertRows([]Row{
			{
				MetricID: 2,
				Labels:   nil,
				DataPoint: DataPoint{
					Value:     float64(counter),
					Timestamp: tick,
				},
			},
		})
	}
	rows, err := db.Select(2, nil, start, start.Add(24*time.Hour))
	assert.NoError(t, err)
	assert.Equal(t, 86400, len(rows))
}

func TestDB(t *testing.T) {
	db, err := New()
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()
	start := time.Now()
	rows := lo.Times(10, func(i int) Row {
		return Row{
			MetricID: 2,
			Labels:   nil,
			DataPoint: DataPoint{
				Value:     float64(i),
				Timestamp: start.Add(time.Duration(i) * time.Second),
			},
		}
	})
	err = db.InsertRows(rows)
	assert.NoError(t, err)
	t.Run("query rows from the database", func(t *testing.T) {
		res, err := db.Select(2, nil, start, start.Add(time.Minute))
		assert.NoError(t, err)
		assert.Len(t, res, len(rows))
		t.Run("with a limit", func(t *testing.T) {
			res, err := db.Select(2, nil, start, start.Add(time.Minute), Limit(1))
			assert.NoError(t, err)
			assert.Len(t, res, 1)
		})
		t.Run("with descending order", func(t *testing.T) {
			res, err := db.Select(2, nil, start, start.Add(time.Minute), Descending())
			assert.NoError(t, err)
			assert.Equal(t, rows[len(rows)-1].Value, res[0].Value)

		})
	})
}
