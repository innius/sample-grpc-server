package scenario

import "context"

type IScenario interface {
	Run(ctx context.Context) error
	ListDimensionKeys() []string
	ListDimensionValues(key string, filter string) []string
	GetMetricByName(dimensions map[string]string, metric string) (Metric, bool)
	ListMetrics(map[string]string) []Metric
}
