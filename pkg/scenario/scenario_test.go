package scenario

import (
	"testing"

	"bitbucket.org/innius/simple-grpc-server/pkg/scenario/specification"
	"github.com/stretchr/testify/assert"
)

func newTestscenario() Scenario {
	spec, _ := specification.New()
	return Scenario{spec: *spec}
}
func TestSimulator_ListDimensionKeys(t *testing.T) {
	sut := newTestscenario()

	res := sut.ListDimensionKeys()
	assert.Len(t, res, 1)
}

func TestSimulator_ListDimensionValues(t *testing.T) {
	sut := newTestscenario()

	res := sut.ListDimensionValues("room", "")
	assert.Len(t, res, 2)
}

func TestSimulator_ListMetrics(t *testing.T) {
	sut := newTestscenario()

	dimensions := map[string]string{"room": "kitchen"}
	res := sut.ListMetrics(dimensions)

	assert.Len(t, res, 1)
}

func TestSimulator_GetMetricByName(t *testing.T) {
	sut := newTestscenario()

	dimensions := map[string]string{"room": "kitchen"}
	res, ok := sut.GetMetricByName(dimensions, "temperature")
	assert.True(t, ok)
	assert.Equal(t, "temperature", res.Name)
}
