module bitbucket.org/innius/simple-grpc-server

go 1.20

replace github.com/apssouza22/grpc-production-go => github.com/innius/grpc-production-go v1.3.0

require (
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0
	github.com/jonboulle/clockwork v0.3.0
	github.com/nakabonne/tstorage v0.3.5
	github.com/samber/lo v1.38.1
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.8.0
	golang.org/x/time v0.1.0
	gonum.org/v1/gonum v0.12.0
	google.golang.org/grpc v1.50.1
	google.golang.org/protobuf v1.28.1
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/objx v0.4.0 // indirect
	golang.org/x/exp v0.0.0-20220303212507-bbda1eaf7a17 // indirect
	golang.org/x/net v0.1.0 // indirect
	golang.org/x/sys v0.1.0 // indirect
	golang.org/x/text v0.4.0 // indirect
	google.golang.org/genproto v0.0.0-20221018160656-63c7b68cfc55 // indirect
)
