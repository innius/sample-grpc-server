# syntax=docker/dockerfile:1
FROM golang:1.20
WORKDIR /go/src/github.com/innius/sample-grpc-server/
COPY . .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .

FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=0 /go/src/github.com/innius/sample-grpc-server/app .
CMD ["./app"]
